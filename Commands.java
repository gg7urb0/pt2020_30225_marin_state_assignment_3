package DB_Commands;

import Connector.Connector;
import Model.Clients;
import Model.Products;
import Main.MainClass;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Commands {
	
    public void add_Client(Clients client, Connector con) throws SQLException {
        
        con.setStat(con.getCon().createStatement());
        con.getStat().executeUpdate("insert into Clients(name,city) values(" +  "'" +client.name + "'" +"," + "'" +client.city +"');");
    }
    
    public void add_Product(Products product,Connector c) throws SQLException {
    	
    	c.setStat(c.getCon().createStatement());
    	ResultSet rs = c.getStat().executeQuery("select * from Products where product_name=" + "'" + product.product_name + "'");
    	if(rs.next() == false) 
            c.getStat().executeUpdate("insert into Products(product_name,quantity,price) values(" +  "'" +product.product_name + "'" +"," +product.quantity +"," + product.price +");");
    	else     
    		c.getStat().executeUpdate("update Products set quantity=" + (rs.getInt(3) + product.quantity) + " where product_name=" + "'" + product.product_name + "';");
    }
 
    public void remove_CLient(Clients client, Connector con) throws SQLException {
        con.setStat(con.getCon().createStatement());
        con.getStat().executeUpdate("delete from Clients where name=" + "'" + client.name + "'" + "and  city=" + "'" + client.city + "'");
    }
  
    public void  remove_Product(Products product,Connector con) throws SQLException {
        con.setStat(con.getCon().createStatement());
        con.getStat().executeUpdate("delete from Products where product_name=" + "'" + product.product_name + "'");
    }
  
    public void get_order(Clients client,Products product,int quantity,Connector con,MainClass obj) throws SQLException {
    	
        con.setStat(con.getCon().createStatement());
        ResultSet rs = con.getStat().executeQuery("select product_name from Products where product_name=" + "'" + product.product_name + "'");
        if(rs.next() == false)
            System.out.println("Produs indisponibil");
        else{
            rs = con.getStat().executeQuery("select quantity from Products where product_name=" + "'" + product.product_name + "'");
            rs.next();
            int cant = rs.getInt(1);
            rs=con.getStat().executeQuery("select price from Products where product_name=" + "'" + product.product_name + "'");
            rs.next();
            double price_aux = rs.getDouble(1);
            if(!(cant - quantity >= 0))
                obj.rez += "Insuficient " + product.product_name + " pentru clientul:" + client.name;
            else{
            	
                System.out.println("Comanda plasata pentru clientul " + client.name);
                con.getStat().executeUpdate("insert into Orders(client_name,product_name,quantity) values(" +  "'" +client.name + "'" +"," + "'" +product.product_name +"'" + "," + quantity +");");
                double price= (double) price_aux * quantity;
               
                con.getStat().executeUpdate("insert into final_orders(client_name,product_name,total_price) values(" +  "'" +client.name + "'" +"," + "'" +product.product_name +"'" + "," + price +");");
           
                if(cant - quantity == 0)
                    this.remove_Product(product,con);
                
                con.getStat().executeUpdate("update Products set quantity=" + (cant - quantity) + " where product_name=" + "'" + product.product_name + "';");
            }
        }
    }
}
