package Model;

public class Orders {
	public int order_id;
    public String client_name;
    public String product_nume;
    public int quantity;

    public Orders(int _order_id, String _client_name, String _product_name, int _quantity) {
        this.order_id = _order_id;
        this.client_name = _client_name;
        this.product_nume = _product_name;
        this.quantity = _quantity;
    }

}
