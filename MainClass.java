package Main;

import DB_Commands.Commands;
import Connector.Connector;
import Model.Clients;
import Model.Products;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.*;
import java.util.Scanner;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class MainClass {
	public String rez;
	public String select_from_Orders(Connector c) throws SQLException {
		String rez = "";
		c.setStat(c.getCon().createStatement());
		ResultSet rs = c.getStat().executeQuery("select *from Final_Orders");
		while (rs.next()) {
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); ++i) {
				if (i > 1) 
					rez += ", ";
				rez += rs.getString(i);
			}
			rez += '\n';
		}
		return rez;
	}

	public String select_from_Client(Connector c) throws SQLException {
		String rez = "";
		c.setStat(c.getCon().createStatement());
		ResultSet rs = c.getStat().executeQuery("select * from Clients");
		while (rs.next()) {
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); ++i) {
				if (i > 1) 
					rez += ", ";
				rez += rs.getString(i);
			}
			rez += '\n';
		}
		return rez;
	}

	public String select_from_Product(Connector c) throws SQLException {
		String rez = "";
		c.setStat(c.getCon().createStatement());
		ResultSet rs = c.getStat().executeQuery("select * from Products");
		while (rs.next()) {
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); ++i) {
				if (i > 1) 
					rez += ", ";
				rez += rs.getString(i);
			}
			rez += '\n';
		}
		return rez;
	}
	public static void main(String[] args) throws ClassNotFoundException, SQLException, FileNotFoundException {
		Connector c = new Connector();
		c.connection_database();
		MainClass o = new MainClass();
		o.rez = "";
		c.con = DriverManager.getConnection(c.url, c.uid, c.pw);
		Commands bus = new Commands();
		Document doc = new Document();
		File f = new File(args[1]);
		Scanner s = new Scanner(f);
		int i1 = 0, i2 = 0, i3 = 0;
		while (s.hasNextLine()) {
			String[] string = new String[4];
			String str = s.nextLine();
			string = str.split("[,:]");
			for (int j = 0; j < string.length; ++j) 
				string[j] = string[j].trim();
			
			if (string[0].equals("Insert client")) {
				Clients cl = new Clients(0, string[1], string[2]);
				bus.add_Client(cl, c);
			} 
			else 
				if (string[0].equals("Insert product")) {
					Products p = new Products(0, string[1], Integer.valueOf(string[2]), Double.valueOf(string[3]));
					bus.add_Product(p, c);
				} 
			else 
				if (string[0].equals("Order")) {
					c.setStat(c.getCon().createStatement());
					ResultSet rs = c.getStat().executeQuery("select * from Clients where name=" + "'" + string[1] + "'");
					rs.next();
					
					Clients cl = new Clients(rs.getInt(1), rs.getString(2), rs.getString(3));
					rs = c.getStat().executeQuery("select * from Products where product_name=" + "'" + string[2] + "'");
					rs.next();
				
					bus.get_order(cl, new Products(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4)), Integer.valueOf(string[3]), c, o);
					try {
						String rez = o.select_from_Orders(c);
						
						PdfWriter.getInstance(doc, new FileOutputStream("Factura.pdf"));
						doc.open();
						doc.add(new Paragraph(rez, FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK)));
							
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				
				} 
			else 
				if (string[0].equals("Delete client")) {
					c.setStat(c.getCon().createStatement());
					ResultSet rs = c.getStat().executeQuery("select * from Clients where name=" + "'" + string[1] + "'");
					rs.next();
					bus.remove_CLient(new Clients(rs.getInt(1), rs.getString(2), rs.getString(3)), c);
				} 
			else 
				if (string[0].equals("Delete product")) {
					c.setStat(c.getCon().createStatement());
					ResultSet rs = c.getStat().executeQuery("select * from Products where product_name=" + "'" + string[1] + "'");
					rs.next();
					bus.remove_Product(new Products(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4)), c);
				}
			else 
				if (string[0].equals("Report client")) {

					try {
						String rez = o.select_from_Client(c);
						Document doc1 = new Document();
						PdfWriter.getInstance(doc1, new FileOutputStream("Client_Raport" + (i1 + 1) + ".pdf"));
						doc1.open();
						doc1.add(new Paragraph(rez, FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK)));
						doc1.close();
						++i1;
	
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				} 
			else
				if (string[0].equals("Report product")) {

				try {
						String rez = o.select_from_Product(c);
						Document doc1 = new Document();
						PdfWriter.getInstance(doc1, new FileOutputStream("Produs_Raport" + (i2 + 1) + ".pdf"));
						doc1.open();
						doc1.add(new Paragraph(rez, FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK)));
						doc1.close();
						++i2;
	
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				} 
			else 
				if (string[0].equals("Report order")) {

					try {
						String rez = o.select_from_Orders(c);
						Document doc1 = new Document();
						PdfWriter.getInstance(doc1, new FileOutputStream("Comanda_Raport" + (i3 + 1) + ".pdf"));
						doc1.open();
						if( o.rez.equals("")) 
							doc1.add(new Paragraph(rez, FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK)));
						else 
							doc1.add(new Paragraph( o.rez, FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK)));
						doc1.close();
						++i3;
	
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				}
         
		}
		doc.close();
	}
}

