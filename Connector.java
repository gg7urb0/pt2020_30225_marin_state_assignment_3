package Connector;

import java.sql.*;


public class Connector {

    public String url = "jdbc:mysql://localhost/order_management?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    public String uid = "root";
    public String pw = "1qa2ws3ed";
    public Connection con;
    public Statement stat;
    public ResultSet result;

    public Connection getCon() {
        return con;
    }


    public Statement getStat() {
        return stat;
    }

    public void setStat(Statement stat) {
        this.stat = stat;
    }

    public void connection_database() throws ClassNotFoundException {
        Class.forName("java.sql.Driver");
    }


}
