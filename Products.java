package Model;

public class Products {
    public int id;
    public String product_name;
    public int quantity;
    public double price;

    public Products(int _id, String _product_name, int _quantity, double _price) {
        this.id = _id;
        this.product_name = _product_name;
        this.quantity = _quantity;
        this.price = _price;
    }

    
}
